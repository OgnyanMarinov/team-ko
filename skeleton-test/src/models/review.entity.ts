import { ManyToOne, Entity, PrimaryGeneratedColumn, Column, OneToMany } from "typeorm";
import { User } from "./user.entity";
import { Book } from "./book.entity";
import { Reaction } from "./reactions.entity";

@Entity()
export class Review {
    @PrimaryGeneratedColumn('increment')
    id: number;
    @Column({ nullable: false, length: 500 })
    content: string;

    @Column()
    bookId: number;
    
    @Column({ default: false })
    isDeleted: boolean;

    @ManyToOne(
        () => User,
        user => user.reviews
    )
    author: User;

    @ManyToOne(
        () => Book,
        book => book.reviews
    )
    book: Book;

    @OneToMany(
        () => Reaction,
        reaction => reaction.review
    )
    reactions: Reaction[];

   
}