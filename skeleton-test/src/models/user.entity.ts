import { OneToMany, Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { Review } from './review.entity';
import { UserRole } from 'src/common/user-role.enum';
import { History } from './history.entity';
import { Reaction } from './reactions.entity';


@Entity('user')
export class User {
   
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ nullable: false, length: 25 })
    username: string;
    @Column({ nullable: false })
    password: string;
    @Column({ type: 'enum', enum: UserRole, default: UserRole.Basic })
    userRole: UserRole;
    @Column({ default: false })
    isDeleted: boolean;
    @Column({ default: false })
    isBanned: boolean;
    @Column({ default: null })
    descriptionBan: string;
    @Column({ default: null })
    periodBanned: Date;

    @OneToMany(
        () => Review,
        review => review.author
    )
    reviews: Review[];

    @OneToMany(
        () => History,
        history => history.user
    )
    historyUser: History[];
    
    @OneToMany(
        () => Reaction,
        reaction => reaction.id
    )
    reaction: Reaction;
}