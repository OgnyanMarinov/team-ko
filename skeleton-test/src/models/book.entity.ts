import { OneToMany, Entity, PrimaryGeneratedColumn, Column } from "typeorm";
import { Review } from "./review.entity";
import { BookStatus } from "src/common/book.status";
import { History } from "./history.entity";

@Entity()
export class Book {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ nullable: false, length: 50 })
    title: string;
    @Column({ nullable: false, length: 25 })
    author: string;
    @Column({ nullable: false })
    publishedYear: number;
    @Column({ nullable: false })
    subject: string; // enum 1. Crime, 2. Fiction, 3. Comics ...
    @Column({ type: 'decimal', precision: 5, scale: 2, default: null })
    rating: number;
    @Column({ type: 'enum', enum: BookStatus, default: BookStatus.available })
    isBorrowed: BookStatus;
    @Column({ default: true })
    isListed: boolean;
    @Column({ default: false })
    isDeleted: boolean;

    @OneToMany(
        () => Review,
        review => review.book
    )
    reviews: Review[];

    @OneToMany(
        () => History,
        history => history.book
    )
    historyBook: History[];
}