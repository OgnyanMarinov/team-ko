import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Review } from "./review.entity";
import { User } from "./user.entity";
import { ReactionType } from "src/common/reaction.type";

@Entity()
export class Reaction {
    @PrimaryGeneratedColumn()
    id: number; 
    @Column({ nullable: false })
    userId: number;
    @Column({ nullable: false})
    reviewId: number; 
    @Column({ type: 'enum', enum: ReactionType })
    reactionType: ReactionType; // enum 1. Like 2. Dislike
    @ManyToOne(
        () => Review,
        review => review.reactions
    )
    review: Review; 

    @ManyToOne(
        () => User,
        user => user.id
    )
    user: User;
    
}