import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne } from "typeorm";
import { User } from "./user.entity";
import { Book } from "./book.entity";

@Entity()
export class History {
    @PrimaryGeneratedColumn()
    id: number;
    @Column({ default: false })
    isReturned: boolean;
    @CreateDateColumn()
    dateBorrowed: Date;
    @CreateDateColumn({ nullable: true, default: '0000-00-00 00:00:00' }) // timestamp does not do the trick!
    dateReturned: Date;
    @Column({ default: false })
    wroteReview: boolean;
    @Column({ type: 'decimal', precision: 5, scale: 2, default: false })
    givenRate: number;

    @ManyToOne(
        () => User,
        user => user.historyUser
    )
    user: User;

    @ManyToOne(
        () => Book,
        book => book.historyBook
    )
    book: Book;
}