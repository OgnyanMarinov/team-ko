 export class LibrarySystemError extends Error {
  public constructor(message?: string, public code?: number) {
    super(message);
  }

} 
/* export class LibrarySystemError {
    constructor(message: string, errorType: ErrorType) {
        this.message = message;
        this.errorType = errorType;
    }

    message: string;
    errorType: ErrorType;
}

export enum ErrorType {
    MissingEntity,
    InvalidOperation
}
 */
