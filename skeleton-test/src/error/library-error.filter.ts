import { LibrarySystemError } from "./library-system.error";
import { Catch, ExceptionFilter, ArgumentsHost } from "@nestjs/common";
import { Response } from 'express';



@Catch(LibrarySystemError)
export class LibrarySystemErrorFilter implements ExceptionFilter {
  catch(exception: LibrarySystemError, host: ArgumentsHost) {
     const ctx = host.switchToHttp();    
    const res = ctx.getResponse<Response>();

    res.status(exception.code).json({
      statusCode: exception.code,
      error: exception.message,
    });
  }
} 

/*  const response = host.switchToHttp().getResponse<Response>();


switch (exception.errorType) {
    case ErrorType.MissingEntity: {
        response
            .status(404)
            .json({
                message: 'Not Found',
                description: exception.message
            });

        break;
    }
    case ErrorType.InvalidOperation: {
        response
            .status(463)
            .json({
                message: 'Invalid Operation',
                description: exception.message
            });

        break;
    }

    default: {
        response
            .status(500)
            .json({
                message: 'Internal Server Error',
                description: 'Oops! Something bad happened. Pls try again! : )'
            });

        break;
    }
}

}
} */
 