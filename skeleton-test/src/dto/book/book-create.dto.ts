import { IsNotEmpty, MaxLength, IsBoolean, IsNumber } from "class-validator";

export class BookCreateDTO {
    
    @IsNotEmpty()
    @MaxLength(50)
    title: string;
    @IsNotEmpty()
    @MaxLength(50)
    author: string;
    @IsNotEmpty()
    @IsNumber()
    publishedYear: number;
    @IsNotEmpty()
    subject: string;
    @IsNotEmpty()
    @IsBoolean()
    isListed: boolean;
}