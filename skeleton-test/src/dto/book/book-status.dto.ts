import { IsEnum } from "class-validator";
import { BookStatus } from "src/common/book.status";

export class BookStatusDTO {
    @IsEnum(BookStatus)
    borrowedStatus: BookStatus;
}