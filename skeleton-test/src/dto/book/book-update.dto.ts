import { IsOptional, IsNotEmpty, MaxLength, IsNumber, IsBoolean, Min, Max } from "class-validator";
import { BookStatus } from "src/common/book.status";

export class BookUpdateDTO {
    @IsOptional()
    @IsNotEmpty()
    @MaxLength(50)
    title: string;
    @IsOptional()
    @IsNotEmpty()
    @MaxLength(50)
    author: string;
    @IsOptional()
    @IsNotEmpty()
    @IsNumber()
    publishedYear: number;
    @IsOptional()
    @IsNotEmpty()
    subject: string;
    @IsOptional()
    @IsNotEmpty()
    @IsNumber()
    @Min(1)
    @Max(5)
    rating: number;
    @IsOptional()
    @IsNotEmpty() // @IsEnum(BookStatus, { each: true }) // can be added @IsEnum(entity: object) for validation
    isBorrowed: BookStatus;
    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    isListed: boolean;
    @IsOptional()
    @IsNotEmpty()
    @IsBoolean()
    isDeleted: boolean;

}