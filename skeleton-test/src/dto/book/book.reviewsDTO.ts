import { BookStatus } from "src/common/book.status";

import { ReviewAuthorDTO } from "../review/review-author.dto";

export class BookReviewsDTO {

    id: number;
    title: string;
    author: string;
    publishedYear: number;
    subject: string;
    rating: number;
    isBorrowed: BookStatus;
    reviews: ReviewAuthorDTO[];

    //reactions: Reaction[];

}