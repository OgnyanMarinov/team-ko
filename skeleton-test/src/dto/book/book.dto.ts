import { BookStatus } from "src/common/book.status";
import { IsNotEmpty, MaxLength } from 'class-validator';

export class BookDTO {
    id: number;
    
    @IsNotEmpty()
    @MaxLength(50)
    title: string;
    @IsNotEmpty()
    @MaxLength(50)
    author: string;
    publishedYear: number;
    subject: string;
    rating: number;
    isBorrowed: BookStatus;

}
