import { IsEnum } from "class-validator";
import { ReactionType } from "src/common/reaction.type";

export class CreateReactionsDTO {
    @IsEnum(ReactionType)
    reactionStatus: ReactionType;
}