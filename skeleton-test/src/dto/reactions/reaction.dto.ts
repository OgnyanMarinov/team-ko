import { ReactionType } from "src/common/reaction.type";

export class ReactionDTO {

    reactionId: number;
    userId: number;
    reactionType: ReactionType;

}