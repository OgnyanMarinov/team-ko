import { UserRole } from "src/common/user-role.enum";

export class UserDTO {
    id: number;
    username: string;
    userRole: UserRole;
}