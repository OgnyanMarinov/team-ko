import { IsString, IsOptional, IsNumberString} from 'class-validator';

export class UserBanDTO {
  @IsString()
  @IsOptional()
  public banInfo: string;
  
  @IsOptional()
  @IsNumberString()
  public banTime: number;
}
