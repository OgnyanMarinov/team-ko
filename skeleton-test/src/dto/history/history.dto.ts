import { User } from "src/models/user.entity";
import { Book } from "src/models/book.entity";

export class HistoryDTO {
    id: number;
    isReturned: boolean;
    dateBorrowed: Date;
    dateReturned: Date;
    wroteReview: boolean;
    givenRate: number;
    user: User;
    book: Book;
}