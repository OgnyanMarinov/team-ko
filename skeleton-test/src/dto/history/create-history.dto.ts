import { UserDTO } from "../user/user.dto";
import { BookDTO } from "../book/book.dto";

export class CreateHistoryDTO {
    user: UserDTO;
    book: BookDTO;
}