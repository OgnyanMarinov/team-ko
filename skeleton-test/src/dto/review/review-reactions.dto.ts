import { Reaction } from "../../models/reactions.entity";
import { ReviewAuthorDTO } from "./review-author.dto";

export class ReviewReactionsDTO {
    reviewId: number;
    content: string
    author: ReviewAuthorDTO;
    reactions: Reaction[];
}