import { UserDTO } from "../user/user.dto";
import { BookDTO } from "../book/book.dto";

export class ReturnReviewDTO {
    id: number;
    content: string;
    author: UserDTO;
    book: BookDTO;

    
    

}