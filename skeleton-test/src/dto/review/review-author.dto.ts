import { UserDTO } from "../user/user.dto";
import { ReactionDTO } from "../reactions/reaction.dto";



export class ReviewAuthorDTO {
    id: number;
    content: string;
    author: UserDTO;
    reactions: ReactionDTO[]

}