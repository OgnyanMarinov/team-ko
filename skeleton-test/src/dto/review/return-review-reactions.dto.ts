import { ReactionDTO } from "../reactions/reaction.dto";
import { UserDTO } from "../user/user.dto";
import { BookDTO } from "../book/book.dto";

export class ReturnReviewReactionsDTO {
    id: number;
    content: string;
    author: UserDTO;
    book: BookDTO;

    reactions: ReactionDTO[];

    

}