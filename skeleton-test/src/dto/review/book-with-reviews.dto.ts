import { BookDTO } from "../book/book.dto";
import { ReviewAuthorDTO } from "./review-author.dto";

export class BookWithReviewsDTO {
    book: BookDTO;
    reviews: ReviewAuthorDTO[];
    
}