import { Length, IsString } from "class-validator";

export class CreateReviewDTO {

    @IsString()
    @Length(20, 250)
    content: string;
}