import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { ReviewsController } from './reviews.controller';
import { BooksController } from './books.controller';
import { ReactionsController } from './reactions.controller';
import { AuthController } from './auth.controller';
import { UsersController } from './users.controller';


@Module({
    imports: [ServicesModule],
    controllers: [UsersController, ReviewsController, BooksController, ReactionsController, AuthController],
})
export class ControllersModule {}
