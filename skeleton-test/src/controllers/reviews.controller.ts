import { Controller, Post, Param, Body, Patch, Get, Delete, UseGuards, ValidationPipe, ParseIntPipe } from '@nestjs/common';
import { UserId } from 'src/auth/user-id.decorator';
import { CreateReviewDTO } from 'src/dto/review/create-review.dto';
import { ReturnReviewDTO } from 'src/dto/review/return-review.dto';
import { ReviewsService } from 'src/services/reviews.service';
import { ResponseMessageDTO } from 'src/dto/responseMessage.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role.enum';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { ReviewAuthorDTO } from 'src/dto/review/review-author.dto';

@Controller('reviews')
export class ReviewsController {

  constructor(
    private readonly reviewsService: ReviewsService,
  ) { }

  @Get(':bookId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned))
  async bookReviews(
    @Param('bookId', ParseIntPipe) bookId: number, @UserId() loggedUserId: number,
  ): Promise<ReviewAuthorDTO[]> {

    return await this.reviewsService.readBookReview(bookId, loggedUserId);
  }

  @Post(':bookId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic))
  async createReview(
    @UserId() loggedUserId: number,
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) reviewContent: CreateReviewDTO
  ): Promise<ReturnReviewDTO> {

    return await this.reviewsService.createBookReview(loggedUserId, bookId, reviewContent);
  }

  @Patch(':reviewId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic))
  async updateBookReview(
    @UserId() loggedUserId: number,
    @Param('reviewId', ParseIntPipe) reviewId: number,
    @Body(new ValidationPipe({ whitelist: true })) reviewContent: CreateReviewDTO
  ): Promise<ReturnReviewDTO> {

    return await this.reviewsService.updateReview(loggedUserId, reviewId, reviewContent);
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic))
  async deleteBookReview(
    @UserId() loggedUserId: number,
    @Param('id', ParseIntPipe) reviewId: number
  ): Promise<ResponseMessageDTO> {

    await this.reviewsService.deleteReview(loggedUserId, reviewId);

    return { msg: 'Review Deleted!' };
  }


  // *** FOR TESTING PURPOSES ***
  @Get('review/:reviewId')
  async readReview(@UserId() loggedUserId: number, @Param('reviewId') reviewId: number): Promise<ReturnReviewDTO> {

    return await this.reviewsService.readReview(loggedUserId, reviewId);
  }
}
