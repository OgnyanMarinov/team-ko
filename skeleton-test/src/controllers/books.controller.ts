import { Controller, Body, Get, Param, Patch, Delete, Put, Post, UseGuards, ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { UserId } from 'src/auth/user-id.decorator';
import { BooksService } from 'src/services/books.service';
import { BookDTO } from 'src/dto/book/book.dto';
import { BookStatus } from 'src/common/book.status';
import { Book } from 'src/models/book.entity';

import { BookCreateDTO } from 'src/dto/book/book-create.dto';
import { BookUpdateDTO } from 'src/dto/book/book-update.dto';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role.enum';
import { BookStatusDTO } from 'src/dto/book/book-status.dto';



@Controller('books')
export class BooksController {

  constructor(
    private readonly booksService: BooksService,
  ) { }

  @Get()
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned))
  async getBooks(@UserId() loggedUserId: number): Promise<BookDTO[]> {
    return await this.booksService.getAll(loggedUserId);
  }

  @Get(':bookId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned))
  async getBookById(@UserId() loggedUserId: number, @Param('bookId', ParseIntPipe) bookId: number): Promise<BookDTO> {

    return this.booksService.getBookById(bookId, loggedUserId);
  }

  @Patch(':bookId/status')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic))
  async borrowBook(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) status: BookStatusDTO,
    @UserId() loggedUserId: number
  ): Promise<BookDTO> {

    return await this.booksService.borrowBook(bookId, status, loggedUserId);
  }

  // *** Return a book functionality ***
  @Delete(':bookId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic, UserRole.Banned))
  async returnBook(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) status: BookStatusDTO,
    @UserId() loggedUserId: number
  ): Promise<BookDTO> {

    return await this.booksService.returnBook(bookId, status, loggedUserId);
  }

  // *** Rate book functionality ***
  @Put(':bookId/rate')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic))
  async rateBook(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body() rateNumber: BookUpdateDTO, @UserId() loggedUserId: number
  ): Promise<BookDTO> {

    return await this.booksService.bookRating(bookId, rateNumber, loggedUserId);
  }

  // Admin Part
  @Post('create')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin))
  async createBook(
    @Body(new ValidationPipe({ whitelist: true })) bookContent: BookCreateDTO
  ): Promise<Book> {

    return await this.booksService.createBook(bookContent);
  }

  @Patch('update/:bookId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin))
  async editBook(
    @Param('bookId', ParseIntPipe) bookId: number,
    @Body(new ValidationPipe({ whitelist: true })) bookContent: BookUpdateDTO
  ): Promise<Book> {

    return await this.booksService.updateBook(bookId, bookContent);
  }
}
