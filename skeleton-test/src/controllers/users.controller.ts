import { Controller, Post, Body, ValidationPipe, Put, Param, Delete, UseGuards, ParseIntPipe } from '@nestjs/common';
import { UsersService } from 'src/services/users.service';
import { CreateUserDTO } from 'src/dto/user/create-user.dto';
import { UserDTO } from 'src/dto/user/user.dto';
import { UserBanDTO } from 'src/dto/user/user-ban.dto';
import { ResponseMessageDTO } from 'src/dto/responseMessage.dto';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role.enum';
import { BlacklistGuard } from 'src/auth/blacklist.guard';

@Controller('users')
export class UsersController {

    constructor(
        private readonly usersService: UsersService,
    ) { }
    // User registration
    @Post()
    async create(@Body(new ValidationPipe({ whitelist: true })) userData: CreateUserDTO,
    ): Promise<UserDTO> {

        return await this.usersService.createUser(userData);
    }

    // Delete user
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin))
    async deleteUser(@Param('id', ParseIntPipe) userId: number): Promise<ResponseMessageDTO> {
        await this.usersService.deleteUser(userId);

        return { msg: 'User successfully deleted!' }
    }

    // Ban/unban user
    @Put(':id')
    @UseGuards(AuthGuard('jwt'),
        BlacklistGuard,
        new RolesGuard(UserRole.Admin))
    async changeUsersBanStatus(
        @Param('id', ParseIntPipe) userId: number,
        @Body(new ValidationPipe({ whitelist: true })) banInfo: UserBanDTO
    ): Promise<ResponseMessageDTO> {

        return await this.usersService.changeUsersBanStatus(userId, banInfo);
    }
}
