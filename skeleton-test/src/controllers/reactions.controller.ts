import { Controller, Param, Body, Post, ParseIntPipe, UseGuards, ValidationPipe } from '@nestjs/common';
import { UserId } from 'src/auth/user-id.decorator';
import { ResponseMessageDTO } from 'src/dto/responseMessage.dto';
import { ReactionType } from 'src/common/reaction.type';
import { ReactionService } from 'src/services/reaction.service';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from 'src/auth/blacklist.guard';
import { RolesGuard } from 'src/auth/roles.guard';
import { UserRole } from 'src/common/user-role.enum';
import { CreateReactionsDTO } from 'src/dto/reactions/create-reactions.dto';

@Controller('reactions')
export class ReactionsController {

  constructor(

    private readonly reactionService: ReactionService,
  ) { }

  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types

  @Post(':reviewId')
  @UseGuards(AuthGuard('jwt'),
    BlacklistGuard,
    new RolesGuard(UserRole.Admin, UserRole.Basic))
  async addVote(
    @UserId() loggedUserId: number,
    @Param('reviewId', ParseIntPipe) reviewId: number,
    @Body(new ValidationPipe({ whitelist: true })) vote: CreateReactionsDTO
  ): Promise<ResponseMessageDTO> {

    await this.reactionService.addReaction(loggedUserId, reviewId, vote);
    return { msg: 'Your Vote Was Added!' };
  }
}