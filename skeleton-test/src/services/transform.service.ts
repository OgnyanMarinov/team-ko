import { Book } from "src/models/book.entity";
import { BookDTO } from 'src/dto/book/book.dto';
import { Review } from "src/models/review.entity";
import { ReturnReviewDTO } from "src/dto/review/return-review.dto";
import { User } from "src/models/user.entity";
import { UserDTO } from "src/dto/user/user.dto";
import { BookWithReviewsDTO } from "src/dto/review/book-with-reviews.dto";
import { CreateHistoryDTO } from "src/dto/history/create-history.dto";
import { ReviewAuthorDTO } from "src/dto/review/review-author.dto";
import { Reaction } from "src/models/reactions.entity";
import { ReactionDTO } from "src/dto/reactions/reaction.dto";
import { BookReviewsDTO } from "src/dto/book/book.reviewsDTO";
import { ReturnReviewReactionsDTO } from "src/dto/review/return-review-reactions.dto";


export class TransformService {

      showBookReviews(book: Book): BookWithReviewsDTO {       

        return {
            book: this.toBookDTO(book),
            reviews: book.reviews.map(r => this.reviewAuthorDTO(r)),       
        }
    }  
    
    toBookDTO(book: Book): BookDTO {
        return {
            id: book.id,
            title: book.title,
            author: book.author,
            publishedYear: book.publishedYear,
            subject: book.subject, // enum 1. Crime, 2. Fiction, 3. Comics ...
            rating: book.rating,
            isBorrowed: book.isBorrowed, 
        }
    }
      
    toUserDTO(user: User): UserDTO {
        return {
            id: user.id,
            username: user.username,
            userRole: user.userRole,
        }
    }
    
     toReturnReviewDTO(review: Review): ReturnReviewDTO {
        return {
            book: this.toBookDTO(review.book),
            id: review.id,
            content: review.content,
            author: this.toUserDTO(review.author),
           
        }
    }  

    reviewAuthorDTO(review: Review): ReviewAuthorDTO { 
        return {
            id: review.id,
            content: review.content,
            author: this.toUserDTO(review.author),
            reactions: review.reactions.map(r => this.ReactionDTO(r)),
        }
    }

    createHistoryDTO(bookData: Book, userData: User): CreateHistoryDTO {
        return {
            user: this.toUserDTO(userData),
            book: this.toBookDTO(bookData),
        }
    }
    
    ReactionDTO(reaction: Reaction): ReactionDTO {
        return {
            reactionId: reaction.id,
            userId: reaction.userId,
            reactionType: reaction.reactionType
           }
          } 

    ReturnReviewReactionsDTO(review: Review): ReturnReviewReactionsDTO {
        return {
            book: this.toBookDTO(review.book),
            id: review.id,
            content: review.content,
            author: this.toUserDTO(review.author),
            reactions: review.reactions.map(r => this.ReactionDTO(r)),
        }
    }
    
    toBookWithReviewsDTO(book: Book): BookReviewsDTO{
            
            return{
            id: book.id,
            title: book.title,
            author: book.author,
            publishedYear: book.publishedYear,
            subject: book.subject,
            rating: book.rating,
            isBorrowed: book.isBorrowed,
           
            reviews: book.reviews.map(r => this.reviewAuthorDTO(r)),
           
        }
    }
   
}
     