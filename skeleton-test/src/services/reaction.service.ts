import { Injectable } from "@nestjs/common";
import { ValidatorService } from "src/utils/validator";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Reaction } from "src/models/reactions.entity";
import { CreateReactionsDTO } from "src/dto/reactions/create-reactions.dto";


@Injectable()
export class ReactionService {

  constructor(
    
    private readonly validator: ValidatorService,
  
    @InjectRepository(Reaction) private readonly reactionRepository: Repository<Reaction>,
  ) { }
  async addReaction(loggedUserId: number, reviewId: number, vote: CreateReactionsDTO): Promise<Reaction>{
    
        const foundReview = await this.validator.validateReviewId(reviewId);
        
        const user = await this.validator.validateUserId(loggedUserId); 
        
        const newReaction = new Reaction();
        newReaction.reactionType = vote.reactionStatus;
        newReaction.userId = user.id;
        newReaction.reviewId = foundReview.id;

        const doubleReaction = await this.reactionRepository.findOne({
          where: {
            userId: user.id,
            reviewId : newReaction.reviewId
            
          }
        });
        if(doubleReaction){
          //doubleReaction.reactionType === vote;
          await this.reactionRepository.save({ ...doubleReaction, reactionType: vote.reactionStatus });
    
        } else {
        
        return this.reactionRepository.save(newReaction);
        
        }
  }
        
    }
    

  
