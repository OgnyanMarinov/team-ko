import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Repository } from 'typeorm';
import { CreateUserDTO } from 'src/dto/user/create-user.dto';
import { UserDTO } from 'src/dto/user/user.dto';
import * as bcrypt from 'bcrypt';
import { TransformService } from './transform.service';
import { LibrarySystemError } from 'src/error/library-system.error';
import { UserBanDTO } from 'src/dto/user/user-ban.dto';
import { ResponseMessageDTO } from 'src/dto/responseMessage.dto';

@Injectable()
export class UsersService {

    constructor(
        private readonly transformer: TransformService,
        @InjectRepository(User) private readonly usersRepository: Repository<User>,
    ) { }

    async createUser(userData: CreateUserDTO): Promise<UserDTO> {
        // validate that no such username exists
        const searchDuplicateUsername = await this.usersRepository.findOne({
            where: {
                username: userData.username,
            }
        });
        if (searchDuplicateUsername) {
            throw new BadRequestException(`Username \'${userData.username}\' is already taken!`)
        }

        const user = this.usersRepository.create(userData);

        user.password = await bcrypt.hash(user.password, 10);

        const created = await this.usersRepository.save(user);

        return this.transformer.toUserDTO(created);
    }

    async deleteUser(id: number): Promise<void> {
        const foundUser: User = await this.findUserEntityById(id);

        await this.usersRepository.save({
            ...foundUser,
            isDeleted: true,
        });
    }

    async findUserEntityById(id: number): Promise<User> {
        const foundUser: User = await this.usersRepository.findOne({
            where: {
                id,
                isDeleted: false,
            }
        });

        if (!foundUser) {
            throw new BadRequestException('User does not exist');
        }

        return foundUser;
    }

    async changeUsersBanStatus(id: number, banInfo: UserBanDTO): Promise<ResponseMessageDTO> {
        const foundUser: User = await this.findUserEntityById(id);

        if (foundUser.isBanned) {
            const statusUnban = `${foundUser.username} has been unbanned!`;
            
            foundUser.isBanned = false;
            foundUser.descriptionBan = 'Unbanned!'
            foundUser.periodBanned = null;
            foundUser.userRole = 2; // even if he was Admin before, he will start from the bottom after the ban!

            await this.usersRepository.save(foundUser);

            return { msg: statusUnban };

        } else {
            const statusBan = `${foundUser.username} has been banned`;

            const period = banInfo.banTime;
            const periodBanned = new Date(Date.now() + (period * 1000 * 60 * 60 * 24)); //Period is day
            foundUser.isBanned = true;
            foundUser.descriptionBan = banInfo.banInfo;
            foundUser.periodBanned = periodBanned;
            foundUser.userRole = 3;

            await this.usersRepository.save(foundUser);

            return { msg: statusBan };
        }
    }
}
