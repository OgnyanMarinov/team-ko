import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/models/user.entity';
import { Review } from 'src/models/review.entity';
import { Book } from 'src/models/book.entity';
import { BooksService } from './books.service';
import { ReviewsService } from './reviews.service';
import { TransformService } from './transform.service';
import { ValidatorService } from 'src/utils/validator';
import { History } from 'src/models/history.entity';
import { ReactionService } from './reaction.service';
import { Reaction } from 'src/models/reactions.entity';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from 'src/constant/secret';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt-strategy';
import { UsersService } from 'src/services/users.service';
import { Token } from 'src/models/token.entity';
import { TasksService } from './tasks.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([User, Review, Book, History, Reaction, Token]),
            PassportModule,
            JwtModule.register({
                secret: jwtConstants.secret,
                signOptions: {
                    expiresIn: '7d',
                },
            })
        ],
            providers: [UsersService, BooksService, ReviewsService, TransformService, ValidatorService, ReactionService, AuthService, JwtStrategy, TasksService],
            exports: [UsersService, BooksService, ReviewsService, ReactionService, AuthService],
})
export class ServicesModule { }
