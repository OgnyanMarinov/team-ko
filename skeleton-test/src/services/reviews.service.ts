import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Review } from 'src/models/review.entity';
import { Repository } from 'typeorm';
import { CreateReviewDTO } from 'src/dto/review/create-review.dto';
import { ReturnReviewDTO } from 'src/dto/review/return-review.dto';
import { Book } from 'src/models/book.entity';
import { User } from 'src/models/user.entity';
import { TransformService } from './transform.service';
import { ValidatorService } from 'src/utils/validator';
import { History } from 'src/models/history.entity';

import { ReviewAuthorDTO } from 'src/dto/review/review-author.dto';


@Injectable()
export class ReviewsService {

  constructor(
    private readonly transformer: TransformService,
    private readonly validator: ValidatorService,
    @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
    @InjectRepository(History) private readonly historyRepository: Repository<History>,

  ) { }

  async readBookReview(bookId: number, loggedUserId: number): Promise<ReviewAuthorDTO[]> {
        
    const user: User = await this.validator.validateUserId(loggedUserId);
    if (user.userRole === 1) {
        const foundReview = await this.reviewRepository.find({
          where: {
            bookId: bookId,
          },
          relations: ['reactions', 'author'],
        });
    
        if (foundReview.length === 0) {
          throw new BadRequestException('No such reviews found!');
        }
    
        return foundReview.map(r=> this.transformer.reviewAuthorDTO(r));
      }
      if (user.userRole === 2 || user.userRole === 3) {
        const foundReview = await this.reviewRepository.find({
            where: {
              bookId: bookId,
              isDeleted: false,
            },
            relations: ['reactions', 'author'],
          });
      
          if (foundReview.length === 0) {
            throw new BadRequestException('No such reviews found!');
          }
      
          return foundReview.map(r=> this.transformer.reviewAuthorDTO(r));
        }
    }

  // *** Create book review functionality ***
  async createBookReview(loggedUserId: number, bookId: number, reviewContent: CreateReviewDTO): Promise<ReturnReviewDTO> {
    // check the book ID
    const foundBook: Book = await this.validator.validateBookId(bookId);
    // check user by ID
    const user: User = await this.validator.validateUserId(loggedUserId);

    // check if the current user borrowed the book and returned it
    const findUser = await this.validator.validateUserId(loggedUserId);
    const findBorrowedBookFromUser =
      await this.historyRepository.find({
        where: {
          user: findUser,
          book: foundBook,
          isReturned: true,
        }
      });

    if (findBorrowedBookFromUser.length === 0) {
      throw new BadRequestException('Either not returned or not the user that borrowed it!');
    }

    // 1.1 Check for reviewing again the same book
    const doubleReview = await this.reviewRepository.find({
      where: {
        author: user,
        book: foundBook,
      }
    });
    if (doubleReview.length !== 0) {
      throw new BadRequestException('You have reviewed this book before!');
    }

    const createReview = this.reviewRepository.create(reviewContent);

    // save the review to the given book ID and to the signed user ID
    createReview.author = user;
    createReview.book = foundBook;
    createReview.bookId = foundBook.id;
    const newReview = await this.reviewRepository.save(createReview);
    findBorrowedBookFromUser[0].wroteReview = true;
    await this.historyRepository.save(findBorrowedBookFromUser);

    return this.transformer.toReturnReviewDTO(newReview);
  }

  async updateReview(loggedUserId: number, reviewId: number, reviewContent: CreateReviewDTO): Promise<ReturnReviewDTO> {
    // check the book ID
    const foundReview: Review = await this.validator.validateReviewId(reviewId);
    // check the user Data
    const user: User = await this.validator.validateUserId(loggedUserId);

    if (user.userRole === 2 && loggedUserId !== foundReview.author.id) {
      throw new BadRequestException('You are not the author of this review and cannot edit it!');
    }

    foundReview.content = reviewContent.content;
    const updatedReview = await this.reviewRepository.save(foundReview);

    return this.transformer.toReturnReviewDTO(updatedReview);
  }

  async deleteReview(loggedUserId: number, reviewId: number): Promise<ReturnReviewDTO> {
    // check the book ID
    const foundReview: Review = await this.validator.validateReviewId(reviewId);
    // check the user Data
    const user: User = await this.validator.validateUserId(loggedUserId);

    if (user.userRole === 2 && loggedUserId !== foundReview.author.id) {
      throw new BadRequestException('You are not the author of this review and cannot edit it!');
    }

    const findHistoryData = await this.historyRepository.find({
      where: {
        user: foundReview.author,
        book: foundReview.book,
      }
    });
    findHistoryData[0].wroteReview = false;
    await this.historyRepository.save(findHistoryData);

    foundReview.isDeleted = true;
    const updatedReview = await this.reviewRepository.save(foundReview);

    return this.transformer.toReturnReviewDTO(updatedReview);
  }

  
  //ТESTING ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  async readReview(loggedUserId: number, reviewId: number): Promise<ReturnReviewDTO> {
    // check the book ID
    const foundreview = await this.validator.validateReviewId(reviewId)
    return this.transformer.ReturnReviewReactionsDTO(foundreview);
  }
}
