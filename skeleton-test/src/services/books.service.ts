import { Injectable, BadRequestException, NotFoundException } from '@nestjs/common';
import { Book } from 'src/models/book.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Not } from 'typeorm';
import { TransformService } from './transform.service';
import { BookDTO } from 'src/dto/book/book.dto';
import { BookStatus } from 'src/common/book.status';
import { User } from 'src/models/user.entity';
import { ValidatorService } from 'src/utils/validator'
import { History } from 'src/models/history.entity';
import { BookCreateDTO } from 'src/dto/book/book-create.dto';
import { BookUpdateDTO } from 'src/dto/book/book-update.dto';
import { BookStatusDTO } from 'src/dto/book/book-status.dto';

@Injectable()
export class BooksService {
    constructor(
        private readonly transformer: TransformService,
        private readonly validator: ValidatorService,
        @InjectRepository(Book) private readonly booksRepository: Repository<Book>,

        @InjectRepository(History) private readonly historyRepository: Repository<History>,
    ) { }

    async getAll(loggedUserId?: number): Promise<BookDTO[]> {
        const user: User = await this.validator.validateUserId(loggedUserId);

        if (user.userRole === 1) {
            const books = await this.booksRepository.find();
            if (!books) {
                throw new NotFoundException('No books found!')
            } else {
                
                return await this.booksRepository.find();
            }
        }
        if (user.userRole === 2 || user.userRole === 3) {
            const books = await this.booksRepository.find({
                isDeleted: false,
                isListed: true,
            });
            if (!books) {
                throw new NotFoundException('No books found!')
            } else {

                return books.map(book => this.transformer.toBookDTO(book));
            }
        }
    }

    async getBookById(bookId: number, loggedUserId: number): Promise<BookDTO> {
        const user: User = await this.validator.validateUserId(loggedUserId);

        if (user.userRole === 1) {
            const foundBook = await this.booksRepository.findOne(bookId);
            if (!foundBook) {
                throw new NotFoundException('No such book found!')
            } else {

                return this.transformer.toBookDTO(foundBook)
            }
        }
        if (user.userRole === 2 || user.userRole === 3) {
            const foundBook = await this.booksRepository.findOne(bookId);
            if (!foundBook) {
                throw new NotFoundException('No such book found!')
            } else {
                const book = await this.booksRepository.findOne({
                    id: bookId,
                    isDeleted: false,
                    isListed: true,
                });

                return this.transformer.toBookDTO(book)
            }
        }
    }

    async borrowBook(bookId: number, status: BookStatusDTO, loggedUserId: number): Promise<BookDTO> {
        // validate status is the correct enum
        if (!(status.borrowedStatus === BookStatus.notAvailable)) {
            throw new BadRequestException('You can only borrow a book from here. Please enter a valid status!');
        }
        const foundBook = await this.validator.validateBookId(bookId);
        // validate book is not borrowed already
        if (foundBook.isBorrowed === status.borrowedStatus) {
            throw new BadRequestException('This book is already borrowed!')
        }

        const updatedBook = await this.booksRepository.save({ ...foundBook, isBorrowed: status.borrowedStatus });
        const findUser = await this.validator.validateUserId(loggedUserId);

        const historyEntry = this.transformer.createHistoryDTO(foundBook, findUser);
        await this.historyRepository.save(historyEntry);

        return this.transformer.toBookDTO(updatedBook);
    }

    // *** Return a book functionality ***
    async returnBook(bookId: number, status: BookStatusDTO, loggedUserId: number): Promise<BookDTO> {
        // validate status is the correct enum
        if (!(status.borrowedStatus === BookStatus.available)) {
            throw new BadRequestException('You can only borrow a book from here. Please enter a valid status!');
        }
        // find the book by ID
        const foundBook = await this.validator.validateBookId(bookId);

        // check if the current user borrowed this book
        const foundUser = await this.validator.validateUserId(loggedUserId);
        const findBorrowedBookFromUser =
            await this.historyRepository.find({
                where: {
                    user: foundUser,
                    book: foundBook,
                    isReturned: false,
                }
            });
        if (findBorrowedBookFromUser.length === 0) {
            throw new BadRequestException('You did not borrow this book! Therefore, you cannot return it!')
        }

        // validate book is not borrowed already
        if (foundBook.isBorrowed === status.borrowedStatus) {
            throw new BadRequestException('This book has already been returned!')
        }

        // update status
        const updatedBook = await this.booksRepository.save({ ...foundBook, isBorrowed: status.borrowedStatus });
        findBorrowedBookFromUser[0].isReturned = true;
        findBorrowedBookFromUser[0].dateReturned = new Date();
        await this.historyRepository.save(findBorrowedBookFromUser);

        return this.transformer.toBookDTO(updatedBook);
    }

    // *** Rate book functionality ***
    async bookRating(bookId: number, rateNumber: BookUpdateDTO, loggedUserId: number): Promise<BookDTO> {
        const foundBook = await this.validator.validateBookId(bookId);
        const foundUser = await this.validator.validateUserId(loggedUserId);

        // user should have borrowed and returned the book
        const findBorrowedBookFromUser =
            await this.historyRepository.find({
                where: {
                    user: foundUser,
                    book: foundBook,
                    isReturned: true,
                    wroteReview: true,
                    givenRate: false,
                }
            });

        if (findBorrowedBookFromUser.length === 0) {
            throw new BadRequestException('Invalid action! Please, change something!')
        }

        findBorrowedBookFromUser[0].givenRate = rateNumber.rating;
        await this.historyRepository.save(findBorrowedBookFromUser);

        const findArrayOfAllRatings =
            await this.historyRepository.find({
                where: {
                    book: foundBook,
                    isReturned: true,
                    wroteReview: true,
                    givenRate: Not(false),
                }
            });

        const sumAllRatings = findArrayOfAllRatings.reduce((acc, arr) => {
            return +arr.givenRate + acc
        }, 0);

        foundBook.rating = Math.round((sumAllRatings / findArrayOfAllRatings.length) * 100) / 100;
        const updatedBookRating = await this.booksRepository.save(foundBook);

        return this.transformer.toBookDTO(updatedBookRating);
    }

    async createBook(bookContent: BookCreateDTO): Promise<Book> {
        const searchDuplicateBook = await this.booksRepository.findOne({
            where: {
                title: bookContent.title,
            }
        });
        if (searchDuplicateBook) {
            throw new BadRequestException(`Book with title \'${bookContent.title}\' already exists!`)
        }
        const book = this.booksRepository.save(bookContent);

        return book;
    }
    async updateBook(bookId: number, bookContent: BookUpdateDTO): Promise<Book> {
        const foundBook = await this.booksRepository.findOne({ where: { id: bookId } });
        const updatedBook = await this.booksRepository.save({ ...foundBook, ...bookContent });

        return updatedBook;
    }
}
