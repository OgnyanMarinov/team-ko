import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm'
import { ControllersModule } from './controllers/controllers.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'komsho', // 1234 // komsho
      database: 'skeletondb',
      entities: ["dist/**/*.entity{.ts,.js}"],
      synchronize: true,
    }),
    ControllersModule,
    ScheduleModule.forRoot(),
  ],
  
})
export class AppModule { }
