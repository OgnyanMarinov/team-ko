import { createParamDecorator, ExecutionContext } from "@nestjs/common";
import { User } from "src/models/user.entity";
import { Request } from 'express';

 export const UserId = createParamDecorator(
    (data: any, ctx: ExecutionContext) => {
        
        const request = ctx.switchToHttp().getRequest<Request>(); // || (ctx as any).user
        const user = request.user as User; // not sure if we need this?!?
        const loggedUserId = user && user.id; 
        
        return loggedUserId;
    },
);
 