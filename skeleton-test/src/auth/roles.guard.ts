import { Injectable, CanActivate, ExecutionContext } from "@nestjs/common";
import { Request } from 'express';
import { UserRole } from "src/common/user-role.enum";
import { User } from "src/models/user.entity";

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(
        role: UserRole,
        secondRole?: UserRole,
        thirdRole?: UserRole,
    ) {
        this.role = role;
        this.secondRole = secondRole;
        this.thirdRole = thirdRole;
    }

    private role: UserRole;
    private secondRole: UserRole;
    private thirdRole: UserRole;

    canActivate(context: ExecutionContext): boolean {
        const request = context.switchToHttp().getRequest<Request>();
        const user = request.user as User;

        return user?.userRole === this.role
            ? true
            : user?.userRole === this.secondRole
                ? true
                : user?.userRole === this.thirdRole;
    }
}

