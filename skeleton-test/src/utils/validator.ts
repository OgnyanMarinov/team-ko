import { Book } from "src/models/book.entity";
import { NotFoundException, BadRequestException } from "@nestjs/common";
import { User } from "src/models/user.entity";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Review } from "src/models/review.entity";
import { LibrarySystemError} from "src/error/library-system.error";

export class ValidatorService {
  constructor(

    @InjectRepository(Book) private readonly booksRepository: Repository<Book>,
    @InjectRepository(Review) private readonly reviewRepository: Repository<Review>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,

  ) { }

  async validateUserId(id: number): Promise<User> {
    const user = await this.userRepository.findOne({
      where: {
        id,
      },//
      //relations: ['reviews', 'book'],
    });
    if (!user) {
       throw new NotFoundException(`No user with id ${id} found!`);
    }

    return user;
  }
  async validateReviewId(reviewId: number): Promise<Review> {
    const foundReview: Review = await this.reviewRepository.findOne({
      where: {
        id: reviewId,
      },
      relations: ['author', 'book', 'reactions'],
    });

    if (foundReview === undefined || foundReview.isDeleted) {
      throw new NotFoundException('No such review found!');
    }

    return foundReview;
  }

  async validateBookId(bookId: number): Promise<Book> {
    const foundBook: Book = await this.booksRepository.findOne({
      where: { id: bookId },
      relations: ['reviews', 'reviews.author', 'reviews.reactions']
    });

    if (foundBook === undefined || foundBook.isDeleted || !foundBook.isListed) {
      throw new NotFoundException('No such book found!');
    }

    return foundBook;
  }
}
